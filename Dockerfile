# Using python 2 image, version 3 fails db migrations
FROM python:2.7

# Setting working directory
WORKDIR /usr/src/app

# Copy and install requirements
COPY requirements.txt ./
RUN pip install -r requirements.txt

# Copy whole project
COPY . .

# Run migrations
RUN python manage.py migrate

# Run tests
RUN python manage.py test

# Exposing port
EXPOSE 8000